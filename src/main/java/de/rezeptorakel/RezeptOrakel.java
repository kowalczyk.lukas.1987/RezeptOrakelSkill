package de.rezeptorakel;

import java.util.ArrayList;
import java.util.List;

import com.amazon.ask.Skill;
import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;

import de.rezeptorakel.handler.CancelandStopIntentHandler;
import de.rezeptorakel.handler.HelpIntentHandler;
import de.rezeptorakel.handler.LaunchRequestHandler;
import de.rezeptorakel.handler.NaechstesRezeptHandler;
import de.rezeptorakel.handler.PermissionExceptionHandler;
import de.rezeptorakel.handler.RezeptAusgewaehltHandler;
import de.rezeptorakel.handler.RezeptSucheHandler;
import de.rezeptorakel.handler.SessionEndedRequestHandler;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class RezeptOrakel extends SkillStreamHandler {

    private static final String AMAZON_ID = "AMAZON_ID";

    public static final String SKILL_TITEL = "Rezept Orakel";

    @SuppressWarnings("unchecked")
    private static Skill getSkill() {
	return Skills.standard().addExceptionHandler(new PermissionExceptionHandler())
		.addRequestHandlers(new CancelandStopIntentHandler(), new RezeptAusgewaehltHandler(),
			new NaechstesRezeptHandler(), new RezeptSucheHandler(), new HelpIntentHandler(),
			new LaunchRequestHandler(), new SessionEndedRequestHandler())
		.withSkillId(System.getenv(AMAZON_ID)).build();
    }

    public RezeptOrakel() {
	super(getSkill());
    }

    /**
     * Es wird eine Liste an benoetigten Berechtigungen zurueck gegeben,<br>
     * die wichtig sind um den Skill zuverwenden
     * 
     * @return Die Liste der Berechtigungen
     */
    public static List<String> getBenoetigtePermissions() {
	List<String> list = new ArrayList<>();
	list.add("read::alexa:household:list");
	list.add("write::alexa:household:list");
	list.add("alexa::profile:email:read");
	return list;
    }
}
