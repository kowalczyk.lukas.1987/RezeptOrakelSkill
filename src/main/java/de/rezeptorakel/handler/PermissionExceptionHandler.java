package de.rezeptorakel.handler;

import java.util.Optional;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.exception.handler.GenericExceptionHandler;

import de.rezeptorakel.RezeptOrakel;
import de.rezeptorakel.datatype.SpeechText;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class PermissionExceptionHandler implements GenericExceptionHandler<HandlerInput, Optional<Response>> {
    @Override
    public boolean canHandle(HandlerInput input, Throwable throwable) {
	if (input == null || throwable == null) {
	    return false;
	}
	return ExceptionUtils.getMessage(throwable)
		.contains("The authentication token does not have access to resource");
    }

    @Override
    public Optional<Response> handle(HandlerInput input, Throwable throwable) {
	String speechText = SpeechText.PERMISSION_FEHLET.getSpeechText();
	return input.getResponseBuilder().withSpeech(speechText)
		.withAskForPermissionsConsentCard(RezeptOrakel.getBenoetigtePermissions()).build();
    }

}
