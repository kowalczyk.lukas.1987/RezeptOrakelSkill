package de.rezeptorakel.handler;

import static com.amazon.ask.request.Predicates.intentName;
import static de.rezeptorakel.datatype.SkillSessionAttributeNames.ANZAHL_PORTIONEN;
import static de.rezeptorakel.datatype.SkillSessionAttributeNames.GEFUNDENE_REZEPTE;
import static de.rezeptorakel.datatype.SkillSessionAttributeNames.GEFUNDENE_REZEPTE_INDEX;
import static de.rezeptorakel.datatype.SkillSlotNames.ANZAHLPORTIONEN;
import static de.rezeptorakel.datatype.SkillSlotNames.MAHLZEIT;
import static de.rezeptorakel.datatype.SkillSlotNames.SCHWEREGRAD;
import static de.rezeptorakel.datatype.SpeechText.KEIN_REZEPT_GEFUNDEN;
import static de.rezeptorakel.datatype.SpeechText.REZEPT_GEFUNEN;
import static de.rezeptorakel.datatype.SpeechText.REZEPT_SUCHE_PROGRESSIV;
import static de.rezeptorakel.datatype.SpeechTextPlaceHolder.REZEPTTITEL;
import static de.rezeptorakel.datatype.SpeechTextPlaceHolder.SUCHPARAMETER;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;

import de.rezeptorakel.RezeptOrakel;
import de.rezeptorakel.datatype.Mahlzeit;
import de.rezeptorakel.datatype.Rezept;
import de.rezeptorakel.datatype.Schweregrad;
import de.rezeptorakel.service.ProgressiveResponseService;
import de.rezeptorakel.service.RezeptSucheService;
import de.rezeptorakel.service.SessionAttributeService;
import de.rezeptorakel.service.SlotService;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class RezeptSucheHandler implements RequestHandler {

    private static final Logger logger = LoggerFactory.getLogger(RezeptSucheHandler.class);

    private static final String INSTANT_NAME = "REZEPT_SUCHE";

    private RezeptSucheService rezeptSuche = RezeptSucheService.getImplementation();

    private SessionAttributeService sessionAttributeService = SessionAttributeService.getImplementation();

    private ProgressiveResponseService progressiveResponseService = ProgressiveResponseService.getImplementation();

    private SlotService slotService = SlotService.getImplementation();

    private String parameter = "";

    public boolean canHandle(HandlerInput input) {
	return input.matches(intentName(INSTANT_NAME));
    }

    public Optional<Response> handle(HandlerInput input) {
	logger.debug("Rezeptsuche starten");
	String speechText = "";
	parameter = "";
	progressiveResponseService.send(input, REZEPT_SUCHE_PROGRESSIV.getSpeechText());
	boolean shouldEndSession = false;
	sessionAttributeService.setSessionAttributes(input);
	slotService.setSlots(input);

	Mahlzeit mahlzeit = ermittleMahlzeit();

	Schweregrad schweregrad = ermittleSchweregrad();

	int anzahlportionen = errmittleAnzahlPortionen();

	if (sessionAttributeService.isSessionAttributEmpty(GEFUNDENE_REZEPTE)) {
	    List<Rezept> rezepte = rezeptSuche.findeRezepte(mahlzeit, schweregrad);
	    if (rezepte.size() > 0) {
		speechText = REZEPT_GEFUNEN.getSpeechText(REZEPTTITEL, rezepte.get(0).getTitel());
	    }
	    sessionAttributeService.putSessionAttribut(input, GEFUNDENE_REZEPTE, rezepte);
	    sessionAttributeService.putSessionAttribut(input, GEFUNDENE_REZEPTE_INDEX, 0);
	    sessionAttributeService.putSessionAttribut(input, ANZAHL_PORTIONEN, anzahlportionen);
	}
	if (StringUtils.isBlank(speechText)) {
	    speechText = KEIN_REZEPT_GEFUNDEN.getSpeechText(SUCHPARAMETER, parameter);
	    shouldEndSession = true;
	}
	return input.getResponseBuilder().withSpeech(speechText).withReprompt(speechText)
		.withSimpleCard(RezeptOrakel.SKILL_TITEL, speechText).withShouldEndSession(shouldEndSession).build();
    }

    private int errmittleAnzahlPortionen() {
	int anzahlportionen = 1;
	if (!slotService.isSlotEmpty(ANZAHLPORTIONEN)) {
	    anzahlportionen = slotService.getInteger(ANZAHLPORTIONEN);
	    parameter += " die anzahl portionen " + anzahlportionen + ".";
	} else {
	    // Erfrage Anzahl Portionen
	    parameter += " keine anzahl portionen.";
	}
	return anzahlportionen;
    }

    private Schweregrad ermittleSchweregrad() {
	Schweregrad schweregrad = Schweregrad.EGAL;
	if (!slotService.isSlotEmpty(SCHWEREGRAD)) {
	    schweregrad = Schweregrad.getSchweregradOfWert(slotService.getMappedName(SCHWEREGRAD));
	    parameter += " der Schweregrad " + schweregrad.getWert() + ".";
	} else {
	    // Erfrage Schweregrad
	    parameter += " kein schweregrad.";
	}
	return schweregrad;
    }

    private Mahlzeit ermittleMahlzeit() {
	Mahlzeit mahlzeit = Mahlzeit.JETZT;
	if (!slotService.isSlotEmpty(MAHLZEIT)) {
	    mahlzeit = Mahlzeit.getMahlzeitOfWert(slotService.getMappedName(MAHLZEIT));
	    parameter += " die tageszeit " + mahlzeit.getWert() + ".";
	} else {
	    // Erfrage Tageszeit
	    parameter += " keine tageszeit.";
	}
	return mahlzeit;
    }

}
