package de.rezeptorakel.handler;

import static com.amazon.ask.request.Predicates.intentName;
import static de.rezeptorakel.datatype.SkillSessionAttributeNames.ANZAHL_PORTIONEN;
import static de.rezeptorakel.datatype.SkillSessionAttributeNames.GEFUNDENE_REZEPTE;
import static de.rezeptorakel.datatype.SkillSessionAttributeNames.GEFUNDENE_REZEPTE_INDEX;
import static de.rezeptorakel.datatype.SpeechText.EMAIL_SENDEN_FEHLER;
import static de.rezeptorakel.datatype.SpeechText.REZEPT_GESENDET;
import static de.rezeptorakel.datatype.SpeechText.REZEPT_SENDEN_PROGRESSIV;
import static de.rezeptorakel.datatype.SpeechTextPlaceHolder.REZEPTTITEL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;

import de.rezeptorakel.RezeptOrakel;
import de.rezeptorakel.datatype.Rezept;
import de.rezeptorakel.datatype.Zutat;
import de.rezeptorakel.service.EMailSendenService;
import de.rezeptorakel.service.EinkaufslisteService;
import de.rezeptorakel.service.ProgressiveResponseService;
import de.rezeptorakel.service.SessionAttributeService;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class RezeptAusgewaehltHandler implements RequestHandler {

    private static final String AMAZON_YES_INTENT = "AMAZON.YesIntent";

    private static final Logger logger = LoggerFactory.getLogger(RezeptAusgewaehltHandler.class);

    private EinkaufslisteService einkaufslisteService = EinkaufslisteService.getImplementation();

    private EMailSendenService eMailSendenService = EMailSendenService.getImpementation();

    private SessionAttributeService sessionAttributeService = SessionAttributeService.getImplementation();

    private ProgressiveResponseService progressiveResponseService = ProgressiveResponseService.getImplementation();

    @Override
    public boolean canHandle(HandlerInput input) {
	return input.matches(intentName(AMAZON_YES_INTENT));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
	logger.debug("Es wurde ein Rezept ausgewählt");
	sessionAttributeService.setSessionAttributes(input);
	Rezept rezept = getAusgewaehltesRezept();
	progressiveResponseService.send(input,
		REZEPT_SENDEN_PROGRESSIV.getSpeechText(REZEPTTITEL, rezept.getTitel()));

	einkaufslisteService.setListManagementServiceClient(input);

	String speechText = "";
	boolean shouldEndSession = false;

	List<Zutat> zutaten = getEinkaufsliste(rezept);

	sessionAttributeService.putSessionAttribut(input, GEFUNDENE_REZEPTE, null);
	sessionAttributeService.putSessionAttribut(input, GEFUNDENE_REZEPTE_INDEX, null);

	einkaufslisteService.fuegeZurEinkaufslisteHinzu(zutaten);
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(rezept, zutaten, input);
	    speechText = REZEPT_GESENDET.getSpeechText();
	} catch (Exception e) {
	    logger.error("Die Email konnte nicht gesendet werden!", e);
	    speechText = EMAIL_SENDEN_FEHLER.getSpeechText();
	}
	shouldEndSession = true;
	return input.getResponseBuilder().withSpeech(speechText).withReprompt(speechText)
		.withSimpleCard(RezeptOrakel.SKILL_TITEL, speechText).withShouldEndSession(shouldEndSession).build();
    }

    private List<Zutat> getEinkaufsliste(Rezept rezept) {
	Integer anzahlportionen = sessionAttributeService.<Integer>getSessionAttribut(ANZAHL_PORTIONEN);
	List<Zutat> zutaten = rezept.getEinkaufsliste(anzahlportionen);
	return zutaten;
    }

    private Rezept getAusgewaehltesRezept() {
	List<Rezept> rezepte = mappRezepteAusSessionAttribut();
	Integer index = sessionAttributeService.<Integer>getSessionAttribut(GEFUNDENE_REZEPTE_INDEX);
	Rezept rezept = rezepte.get(index);
	return rezept;
    }

    private List<Rezept> mappRezepteAusSessionAttribut() {
	ArrayList<Map<String, Object>> sessionAttributesRezepte = sessionAttributeService
		.<ArrayList<Map<String, Object>>>getSessionAttribut(GEFUNDENE_REZEPTE);
	ArrayList<Rezept> ausg = new ArrayList<>();
	sessionAttributesRezepte.forEach((v) -> ausg.add(Rezept.mappeFromMap(v)));
	return ausg;
    }

}
