package de.rezeptorakel.datatype;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public enum SpeechTextPlaceHolder {
    REZEPTTITEL("rezepttitel"), SUCHPARAMETER("suchparameter");

    private String name;

    private SpeechTextPlaceHolder(String name) {
	this.name = name;
    }

    /**
     * @return Die Variable name im Format ${<i>name</i>}
     */
    public String getPlaceholderName() {
	return "${" + name + "}";
    }

    /**
     * @return Die Variable name
     */
    public String getName() {
	return name;
    }

}
