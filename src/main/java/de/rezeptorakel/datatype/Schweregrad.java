package de.rezeptorakel.datatype;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public enum Schweregrad {

    SCHWER("schwer"), MITTEL("mittel"), EINFACH("einfach"), EGAL("egal");

    private String wert;

    private Schweregrad(String wert) {
	this.wert = wert;
    }

    /**
     * Es wird nach einem passenden Schweregrad gesucht
     * 
     * @param value Der zu suchende Wert, z.B. "einfach"
     * @return Das genfunde Schweregrad-Enum-Element
     */
    public static Schweregrad getSchweregradOfWert(String value) {
	if (value != null) {
	    for (Schweregrad mahlzeit : values()) {
		if (mahlzeit.getWert().equals(value.toLowerCase())) {
		    return mahlzeit;
		}
	    }
	}
	return EGAL;
    }

    /**
     * @return die Variable wert
     */
    public String getWert() {
	return wert;
    }
}
