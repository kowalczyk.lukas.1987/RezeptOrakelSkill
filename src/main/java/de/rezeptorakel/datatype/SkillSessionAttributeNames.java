package de.rezeptorakel.datatype;
/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public enum SkillSessionAttributeNames {
    GEFUNDENE_REZEPTE, GEFUNDENE_REZEPTE_INDEX, ANZAHL_PORTIONEN;
}
