package de.rezeptorakel.datatype;

import org.apache.commons.lang3.StringUtils;

import static de.rezeptorakel.datatype.SpeechTextPlaceHolder.*;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public enum SpeechText {

    REZEPT_SENDEN_PROGRESSIV("ich werde die Zutaten von " + REZEPTTITEL.getPlaceholderName()
	    + " auf deine Einkaufsliste speichern und dir das Rezept als email senden."), //
    PERMISSION_FEHLET(
	    "Um dir rezept und zutaten zukommen zulassen, trage die entsprechenden berechtigungen im skill in der alexa-app ein."), //
    VERABSCHIEDEN("Aufwiedersehn"), //
    HILFE("Frag einfach: \"was soll ich kochen?\" oder sage: \"stop\""), //
    WILLKOMMEN("Willkommen, du kannst mich nun fragen \"was soll ich kochen?\""), //
    KEINE_WEITEREN_REZEPTE("Ich habe leider keine Rezepte mehr, versuch doch mal einen anderen schweregrad"), //
    NAECHSTES_REZEPT_GEFUNDEN("Willst du vielleicht " + REZEPTTITEL.getPlaceholderName() + " kochen?"), //
    KEINE_REZEPTE_GEFUNDEN("Ich habe leider keine Rezepte!"), //
    REZEPT_GESENDET("Die email ist versendet und deine einkaufsliste aktuallisiert. Aufwiedersehen und bis bald "), //
    EMAIL_SENDEN_FEHLER(
	    "leider konnte ich dir keine Email senden. bitte �berpr�fe deine einstellungen in der alexa-app."), //
    KEIN_REZEPT_GEFUNDEN("Ich habe leider nichts gefunden mit deinen Angaben: " + SUCHPARAMETER.getPlaceholderName()), //
    REZEPT_GEFUNEN("Ich habe " + REZEPTTITEL.getPlaceholderName() + " gefunden, willst du das vielleicht kochen?"), //
    REZEPT_SUCHE_PROGRESSIV("Ich suche nach Rezepten")//
    ;

    private String speechText;

    private SpeechText(String speechtext) {
	this.speechText = speechtext;
    }

    /**
     * @return die Variable speechText
     */
    public String getSpeechText() {
	return speechText;
    }

    /**
     * @param placeholder Einen Placeholder, der ersetzt werden soll
     * @param value       Der einzuf�gende Value
     * @return Den ersetzten Text mit den mitgeben Platzhalter und value
     */
    public String getSpeechText(SpeechTextPlaceHolder placeholder, String value) {
	if (StringUtils.isBlank(value)) {
	    throw new IllegalArgumentException("Value darf nicht null oder Leer sein");
	}
	if (!StringUtils.contains(speechText, placeholder.getPlaceholderName())) {
	    throw new IllegalArgumentException("placeholder ist nicht im SpeechText enthalten");
	}
	return StringUtils.replace(speechText, "${" + placeholder.getName() + "}", value);
    }

}
