package de.rezeptorakel.datatype;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public enum SkillSlotNames {
    MAHLZEIT("mahlzeit"), SCHWEREGRAD("schweregrad"), ANZAHLPORTIONEN("anzahlportionen");

    private String slotName;

    private SkillSlotNames(String slotName) {
	this.slotName = slotName;
    }

    /**
     * @return die Variable slotName
     */
    public String getSlotName() {
	return slotName;
    }
}
