package de.rezeptorakel.datatype;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public enum Mahlzeit {

    MITTAG("mittag"), ABEND("abend"), FRUEH("frueh"), JETZT("jetzt");

    private static final Logger logger = LoggerFactory.getLogger(Mahlzeit.class);
    private String wert;

    private Mahlzeit(String wert) {
	this.wert = wert;
    }

    /**
     * Ermittelt die Mahltzeit anhand des value
     * 
     * @param value
     * @return die Malzeit, <br>
     *         <b>dafault</b> ist JETZT
     */
    public static Mahlzeit getMahlzeitOfWert(String value) {
	if (value != null) {
	    for (Mahlzeit mahlzeit : values()) {
		if (mahlzeit.getWert().equals(value.toLowerCase())) {
		    return mahlzeit;
		}
	    }
	}
	return JETZT;
    }

    /**
     * @return die Variable wert
     */
    public String getWert() {
	return wert;
    }

    /**
     * Ermittelt aus der ‹bergebenen Zeit die Mahlzeit
     * 
     * @param localDateTime die zubestimmende Uhrzeit
     * @return Ein Mahlzeit Enum Element, <br>
     *         ansonsten <b>null</b>
     */
    public static Mahlzeit ermittleAktuelleMahlzeit(LocalDateTime localDateTime) {
	DateTimeFormatter df = DateTimeFormatter.ofPattern("HH");
	int tagesStunden = Integer.parseInt(localDateTime.format(df));
	logger.debug("TAGES_STUNDE=" + tagesStunden);
	if (0 <= tagesStunden && tagesStunden < 4) {
	    return ABEND;
	} else if (4 <= tagesStunden && tagesStunden < 12) {
	    return FRUEH;
	} else if (12 <= tagesStunden && tagesStunden < 16) {
	    return MITTAG;
	} else if (16 <= tagesStunden && tagesStunden < 24) {
	    return ABEND;
	}
	return null;
    }
}
