package de.rezeptorakel.service;

import java.util.Map;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;

import de.rezeptorakel.datatype.SkillSessionAttributeNames;
/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class SessionAttributeService {

    private HandlerUtilService handlerUtilService = HandlerUtilService.getImplementation();

    private static SessionAttributeService sessionAttributeService;

    private Map<String, Object> sessionAttributes;

    private SessionAttributeService() {
	sessionAttributes = null;
    }

    /**
     * Gibt ein singelton Instanz diser Klasse wieder zur�ck
     * 
     * @return die singelton Instanz
     */
    public static SessionAttributeService getImplementation() {
	return sessionAttributeService == null ? new SessionAttributeService() : sessionAttributeService;
    }

    /**
     * Es werden die SessionAttribute aus dem input Parameter in dieser Instanz
     * gespeichert.
     * 
     * @param input Ein HandlerInput element
     */
    public void setSessionAttributes(HandlerInput handlerInput) {
	this.sessionAttributes = handlerUtilService.getSessionAttributes(handlerInput);
    }

    /**
     * Ein SessionAttribut aus der Instanz holen.
     * 
     * @param <T>                        Typ der geholt werden soll
     * @param reVorSessionAttributeNames Der Key des Wertes, der geholt werden soll
     * @return Der Wert des gfundenen SessionAttributs, <br>
     *         oder <b>null</b>
     */
    @SuppressWarnings("unchecked")
    public <T> T getSessionAttribut(SkillSessionAttributeNames reVorSessionAttributeNames) {
	if (sessionAttributes != null) {
	    return (T) sessionAttributes.get(reVorSessionAttributeNames.name());
	}
	return null;
    }

    /**
     * Pr�ft ob das SessionAttrbiut ein Wert != null hat.
     * 
     * @param reVorSessionAttributeNames Der Key des Wertes, der �berpr�ft werden
     *                                   soll
     * @return <b>true</b> wenn der Wert != null ist, <br>
     *         ansonsten <b>false</b>
     */
    public boolean isSessionAttributEmpty(SkillSessionAttributeNames reVorSessionAttributeNames) {
	if (sessionAttributes != null) {
	    return sessionAttributes.get(reVorSessionAttributeNames.name()) == null;
	}
	return true;
    }

    /**
     * Es wird ein Wert als SessionAttribut gespeichert
     * 
     * @param reVorSessionAttributeNames Der Key des Wertes, der gespeichert werden
     *                                   soll
     * @param value                      Der Wert, der gespeichert werden soll
     */
    public void putSessionAttribut(HandlerInput handlerInput, SkillSessionAttributeNames reVorSessionAttributeNames, Object value) {
	if (sessionAttributes != null) {
	    sessionAttributes.put(reVorSessionAttributeNames.name(), value);
	    handlerUtilService.setSessionAttributes(handlerInput, sessionAttributes);
	}
    }
}
