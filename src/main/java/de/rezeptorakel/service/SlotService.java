package de.rezeptorakel.service;

import java.util.Map;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Slot;

import de.rezeptorakel.datatype.SkillSlotNames;
/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class SlotService {

    private HandlerUtilService handlerUtilService = HandlerUtilService.getImplementation();

    private static SlotService slotService;

    private Map<String, Slot> slots;

    private SlotService() {
	slots = null;
    }

    /**
     * Gibt ein singelton Instanz diser Klasse wieder zur�ck
     * @return die singelton Instanz
     */
    public static SlotService getImplementation() {
	return slotService == null ? new SlotService() : slotService;
    }

    /**
     * Es werden die Slots aus dem input Parameter in dieser Instanz gespeichert.
     * 
     * @param input Ein HandlerInput element
     */
    public void setSlots(HandlerInput input) {
	this.slots = handlerUtilService.getSlots(input);
    }

    /**
     * Ermittlung des gemappedten Namens eines Slots
     * 
     * @param reVorSlotNames Der zusuchende SlotKey
     * @return den Inhalt des gefunden Slots,<br>
     *         ansonsten <b>null</b>
     */
    public String getMappedName(SkillSlotNames reVorSlotNames) {
	if (slots != null && reVorSlotNames != null) {
	    Slot slot = slots.get(reVorSlotNames.getSlotName());
	    return handlerUtilService.getMappedNameOfSlot(slot);
	}
	return null;
    }

    /**
     * Holt einen Integer aus einem Slot
     * 
     * @param reVorSlotNames Der zusuchende SlotKey
     * @return den Inhalt des gefunden Slots,<br>
     *         ansonsten <b>null</b>
     */
    public Integer getInteger(SkillSlotNames reVorSlotNames) {
	if (slots != null && reVorSlotNames != null) {
	    Slot slot = slots.get(reVorSlotNames.getSlotName());
	    if (slot != null) {
		try {
		    return Integer.valueOf(slot.getValue());
		} catch (NumberFormatException e) {
		    return null;
		}
	    }
	}
	return null;
    }

    /**
     * Pr�ft nach ob der zusuchende SlotKey vorhanden ist
     * 
     * @param reVorSlotNames Der zusuchende SlotKey
     * @return <b>true</b> der Key ist vorhanden, <br>
     *         ansonsten <b>false</b>
     */
    public boolean isSlotEmpty(SkillSlotNames reVorSlotNames) {
	if (slots != null && reVorSlotNames != null) {
	    return !slots.containsKey(reVorSlotNames.getSlotName())
		    || slots.get(reVorSlotNames.getSlotName()).getValue() == null;
	}
	return true;
    }
}
