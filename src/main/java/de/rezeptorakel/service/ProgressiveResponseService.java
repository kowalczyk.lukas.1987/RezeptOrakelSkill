package de.rezeptorakel.service;

import org.apache.commons.lang3.StringUtils;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;

/**
 * 
 * @author Lukas Kowalczyk
 *
 */
public class ProgressiveResponseService {

    private HandlerUtilService handlerUtilService = HandlerUtilService.getImplementation();

    private static ProgressiveResponseService progressiveResponseService;

    private ProgressiveResponseService() {
    }

    public static ProgressiveResponseService getImplementation() {
	return progressiveResponseService == null ? new ProgressiveResponseService() : progressiveResponseService;
    }

    /**
     * Versendet einen ProgressiveResponse an den Anwender
     * 
     * @param handlerInput Ein HandlerInput element
     * @param speechText Der Text dem der Anwender gesendet werden soll
     */
    public void send(HandlerInput handlerInput, String speechText) {
	if (handlerInput == null) {
	    throw new IllegalArgumentException("handlerInput is null");
	}
	if (StringUtils.isEmpty(speechText)) {
	    throw new IllegalArgumentException("speechText is empty");
	}
	handlerUtilService.sendProgressiveResponse(handlerInput, speechText);

    }
}
