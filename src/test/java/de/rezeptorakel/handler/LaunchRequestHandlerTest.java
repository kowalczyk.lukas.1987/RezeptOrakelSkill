package de.rezeptorakel.handler;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class LaunchRequestHandlerTest {

    private LaunchRequestHandler launchRequestHandler = new LaunchRequestHandler();

    @Mock
    private HandlerInput handlerInput;

    @Test
    public void canhandle() throws Exception {
	try {
	    launchRequestHandler.canHandle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.matches(any())).thenReturn(true);
	assertTrue(launchRequestHandler.canHandle(handlerInput));

	Mockito.lenient().when(handlerInput.matches(any())).thenReturn(false);
	assertFalse(launchRequestHandler.canHandle(handlerInput));

    }

    @Test
    public void handle() throws Exception {
	try {
	    launchRequestHandler.handle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}

	ResponseBuilder responseBuilder = new ResponseBuilder();
	Mockito.lenient().when(handlerInput.getResponseBuilder()).thenReturn(responseBuilder);

	Optional<Response> response = launchRequestHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString().indexOf("Willkommen, du kannst mich nun fragen") > -1);

    }

}
