package de.rezeptorakel.handler;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;

import de.rezeptorakel.datatype.SkillSessionAttributeNames;
import de.rezeptorakel.service.EMailSendenService;
import de.rezeptorakel.service.EinkaufslisteService;
import de.rezeptorakel.service.ProgressiveResponseService;
import de.rezeptorakel.service.SessionAttributeService;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class RezeptAusgewaehltHandlerTest {

    private RezeptAusgewaehltHandler rezeptAusgewaehltHandler = new RezeptAusgewaehltHandler();

    private static final String TITEL = "titel";

    private static final String ZUTATEN = "zutaten";

    private static final String SCHWEREGRAD = "schweregrad";

    private static final String ANLEITUNG = "anleitung";

    private static final String MAHLZEIT = "mahlzeit";

    private static final String ID = "ID";

    @Mock
    private HandlerInput handlerInput;

    @Mock
    private SessionAttributeService sessionAttributeService;

    @Mock
    private EinkaufslisteService einkaufslisteService;

    @Mock
    private EMailSendenService eMailSendenService;

    @Mock
    private ProgressiveResponseService progressiveResponseService;

    @Test
    public void canhandle() throws Exception {
	try {
	    rezeptAusgewaehltHandler.canHandle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.matches(any())).thenReturn(true);
	assertTrue(rezeptAusgewaehltHandler.canHandle(handlerInput));

	Mockito.lenient().when(handlerInput.matches(any())).thenReturn(false);
	assertFalse(rezeptAusgewaehltHandler.canHandle(handlerInput));
    }

    @Test
    public void handle() throws Exception {
	try {
	    rezeptAusgewaehltHandler.handle(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getResponseBuilder()).thenReturn(new ResponseBuilder());
	doNothing().when(sessionAttributeService).setSessionAttributes(any());

	Mockito.lenient()
		.when(sessionAttributeService
			.<Integer>getSessionAttribut(eq(SkillSessionAttributeNames.GEFUNDENE_REZEPTE_INDEX)))
		.thenReturn(new Integer(0));
	Mockito.lenient()
		.when(sessionAttributeService
			.<Integer>getSessionAttribut(eq(SkillSessionAttributeNames.ANZAHL_PORTIONEN)))
		.thenReturn(new Integer(1));
	Mockito.lenient().when(sessionAttributeService
		.<ArrayList<Map<String, Object>>>getSessionAttribut(eq(SkillSessionAttributeNames.GEFUNDENE_REZEPTE)))
		.thenReturn(generateRezeptGefuellteMap());
	FieldSetter.setField(rezeptAusgewaehltHandler,
		rezeptAusgewaehltHandler.getClass().getDeclaredField("sessionAttributeService"),
		sessionAttributeService);
	FieldSetter.setField(rezeptAusgewaehltHandler,
		rezeptAusgewaehltHandler.getClass().getDeclaredField("einkaufslisteService"), einkaufslisteService);
	FieldSetter.setField(rezeptAusgewaehltHandler,
		rezeptAusgewaehltHandler.getClass().getDeclaredField("eMailSendenService"), eMailSendenService);
	FieldSetter.setField(rezeptAusgewaehltHandler,
		rezeptAusgewaehltHandler.getClass().getDeclaredField("progressiveResponseService"), progressiveResponseService);
	Optional<Response> response = rezeptAusgewaehltHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString()
		.indexOf("Die email ist versendet und deine einkaufsliste") > -1);

	doThrow(new Exception()).when(eMailSendenService).versendeRezeptUndEinkaufsliste(any(), any(), any());
	response = rezeptAusgewaehltHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(
		response.get().getOutputSpeech().toString().indexOf("leider konnte ich dir keine Email senden") > -1);
    }

    private ArrayList<Map<String, Object>> generateRezeptGefuellteMap() {
	HashMap<String, Object> hashMap = new HashMap<String, Object>();
	hashMap.put(ID, "0");
	hashMap.put(MAHLZEIT, "mittag");
	hashMap.put(ANLEITUNG, "anleitung");
	hashMap.put(SCHWEREGRAD, "einfach");
	hashMap.put(ZUTATEN, gefuellteZutaten());
	hashMap.put(TITEL, "titel");
	ArrayList<Map<String, Object>> ausg = new ArrayList<>();
	ausg.add(hashMap);
	ausg.add(hashMap);
	return ausg;
    }

    private List<Map<String, Object>> gefuellteZutaten() {
	ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
	list.add(gefuelltZutat());
	return list;
    }

    private HashMap<String, Object> gefuelltZutat() {
	HashMap<String, Object> hashMap = new HashMap<String, Object>();
	hashMap.put("anzahl", "1");
	hashMap.put("name", "zutat");
	hashMap.put("einheit", "g");
	return hashMap;
    }

}
