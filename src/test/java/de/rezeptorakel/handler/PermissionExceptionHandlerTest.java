package de.rezeptorakel.handler;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class PermissionExceptionHandlerTest {
    private PermissionExceptionHandler permissionExceptionHandler = new PermissionExceptionHandler();

    @Mock
    private HandlerInput handlerInput;

    @Test
    void canhandle() {
	assertFalse(permissionExceptionHandler.canHandle(null, null));

	assertFalse(permissionExceptionHandler.canHandle(handlerInput, null));

	assertFalse(permissionExceptionHandler.canHandle(null, new Exception(
		"ajsdhasjkdaksjhdkashdkasd The authentication token does not have access to resource akdhkajsdaksjdh")));
	assertTrue(permissionExceptionHandler.canHandle(handlerInput, new Exception(
		"ajsdhasjkdaksjhdkashdkasd The authentication token does not have access to resource akdhkajsdaksjdh")));

    }

    @Test
    void handle() {
	try {
	    permissionExceptionHandler.handle(null, null);
	    assertTrue(false);
	} catch (Exception e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getResponseBuilder()).thenReturn(new ResponseBuilder());
	Optional<Response> response = permissionExceptionHandler.handle(handlerInput, null);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString()
		.indexOf("Um dir rezept und zutaten zukommen zulassen") > -1);
    }

}
