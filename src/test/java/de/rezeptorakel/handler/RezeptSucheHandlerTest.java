package de.rezeptorakel.handler;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.services.directive.DirectiveServiceClient;
import com.amazon.ask.response.ResponseBuilder;

import de.rezeptorakel.datatype.Mahlzeit;
import de.rezeptorakel.datatype.Rezept;
import de.rezeptorakel.datatype.Schweregrad;
import de.rezeptorakel.datatype.SkillSessionAttributeNames;
import de.rezeptorakel.datatype.SkillSlotNames;
import de.rezeptorakel.datatype.Zutat;
import de.rezeptorakel.service.ProgressiveResponseService;
import de.rezeptorakel.service.RezeptSucheService;
import de.rezeptorakel.service.SessionAttributeService;
import de.rezeptorakel.service.SlotService;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class RezeptSucheHandlerTest {
    private RezeptSucheHandler rezeptSucheHandler = new RezeptSucheHandler();

    @Mock
    private HandlerInput handlerInput;

    @Mock
    private SessionAttributeService sessionAttributeService;

    @Mock
    private SlotService slotService;

    @Mock
    private RezeptSucheService rezeptSuche;

    @Mock
    private ProgressiveResponseService progressiveResponseService;

    @Mock
    private DirectiveServiceClient directiveServiceClient;

    @Test
    public void canhandle() throws Exception {
	try {
	    rezeptSucheHandler.canHandle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.matches(any())).thenReturn(true);
	assertTrue(rezeptSucheHandler.canHandle(handlerInput));

	Mockito.lenient().when(handlerInput.matches(any())).thenReturn(false);
	assertFalse(rezeptSucheHandler.canHandle(handlerInput));
    }

    @Test
    public void handle() throws Exception {
	try {
	    rezeptSucheHandler.handle(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getResponseBuilder()).thenReturn(new ResponseBuilder());

	Mockito.lenient()
		.when(sessionAttributeService.isSessionAttributEmpty(eq(SkillSessionAttributeNames.GEFUNDENE_REZEPTE)))
		.thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.SCHWEREGRAD))).thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.MAHLZEIT))).thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(true);

	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.SCHWEREGRAD)))
		.thenReturn(Schweregrad.EGAL.getWert());
	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.MAHLZEIT)))
		.thenReturn(Mahlzeit.FRUEH.getWert());
	Mockito.lenient().when(slotService.getInteger(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(0);

	doNothing().when(slotService).setSlots(any());
	

	Mockito.lenient().when(rezeptSuche.findeRezepte(any(), any())).thenReturn(new ArrayList<Rezept>());

	FieldSetter.setField(rezeptSucheHandler,
		rezeptSucheHandler.getClass().getDeclaredField("sessionAttributeService"), sessionAttributeService);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("rezeptSuche"),
		rezeptSuche);
	FieldSetter.setField(rezeptSucheHandler,
		rezeptSucheHandler.getClass().getDeclaredField("progressiveResponseService"), progressiveResponseService);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("slotService"),
		slotService);
	Optional<Response> response = rezeptSucheHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString()
		.indexOf("Ich habe leider nichts gefunden mit deinen Angaben:") > -1);

	Mockito.lenient()
		.when(sessionAttributeService.isSessionAttributEmpty(eq(SkillSessionAttributeNames.GEFUNDENE_REZEPTE)))
		.thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.SCHWEREGRAD))).thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.MAHLZEIT))).thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(true);

	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.SCHWEREGRAD)))
		.thenReturn(Schweregrad.EGAL.getWert());
	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.MAHLZEIT)))
		.thenReturn(Mahlzeit.FRUEH.getWert());
	Mockito.lenient().when(slotService.getInteger(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(0);

	Mockito.lenient().when(rezeptSuche.findeRezepte(any(), any())).thenReturn(generateRezepte());

	FieldSetter.setField(rezeptSucheHandler,
		rezeptSucheHandler.getClass().getDeclaredField("sessionAttributeService"), sessionAttributeService);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("rezeptSuche"),
		rezeptSuche);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("slotService"),
		slotService);

	response = rezeptSucheHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString().indexOf("willst du das vielleicht kochen") > -1);

	Mockito.lenient()
		.when(sessionAttributeService.isSessionAttributEmpty(eq(SkillSessionAttributeNames.GEFUNDENE_REZEPTE)))
		.thenReturn(false);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.SCHWEREGRAD))).thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.MAHLZEIT))).thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(true);

	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.SCHWEREGRAD)))
		.thenReturn(Schweregrad.EGAL.getWert());
	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.MAHLZEIT)))
		.thenReturn(Mahlzeit.FRUEH.getWert());
	Mockito.lenient().when(slotService.getInteger(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(0);

	Mockito.lenient().when(rezeptSuche.findeRezepte(any(), any())).thenReturn(generateRezepte());

	FieldSetter.setField(rezeptSucheHandler,
		rezeptSucheHandler.getClass().getDeclaredField("sessionAttributeService"), sessionAttributeService);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("rezeptSuche"),
		rezeptSuche);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("slotService"),
		slotService);
	response = rezeptSucheHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString()
		.indexOf("Ich habe leider nichts gefunden mit deinen Angaben:") > -1);

	Mockito.lenient()
		.when(sessionAttributeService.isSessionAttributEmpty(eq(SkillSessionAttributeNames.GEFUNDENE_REZEPTE)))
		.thenReturn(true);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.SCHWEREGRAD))).thenReturn(false);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.MAHLZEIT))).thenReturn(false);
	Mockito.lenient().when(slotService.isSlotEmpty(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(false);

	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.SCHWEREGRAD)))
		.thenReturn(Schweregrad.EGAL.getWert());
	Mockito.lenient().when(slotService.getMappedName(eq(SkillSlotNames.MAHLZEIT)))
		.thenReturn(Mahlzeit.FRUEH.getWert());
	Mockito.lenient().when(slotService.getInteger(eq(SkillSlotNames.ANZAHLPORTIONEN))).thenReturn(0);

	Mockito.lenient().when(rezeptSuche.findeRezepte(any(), any())).thenReturn(generateRezepte());

	FieldSetter.setField(rezeptSucheHandler,
		rezeptSucheHandler.getClass().getDeclaredField("sessionAttributeService"), sessionAttributeService);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("rezeptSuche"),
		rezeptSuche);
	FieldSetter.setField(rezeptSucheHandler, rezeptSucheHandler.getClass().getDeclaredField("slotService"),
		slotService);
	response = rezeptSucheHandler.handle(handlerInput);
	assertTrue(response.isPresent());
	assertTrue(response.get().getOutputSpeech().toString().indexOf("willst du das vielleicht kochen") > -1);
    }

    private List<Rezept> generateRezepte() {
	ArrayList<Rezept> ausg = new ArrayList<>();
	Rezept r = new Rezept();
	r.setAnleitung("anleitung");
	r.setId(1);
	r.setMahlzeit("mittag");
	r.setSchweregrad("einfach");
	r.setTitel("titel");
	r.setZutaten(generateZutaten());
	ausg.add(r);
	return ausg;
    }

    private List<Zutat> generateZutaten() {
	ArrayList<Zutat> zutaten = new ArrayList<>();
	Zutat z = new Zutat();
	z.setAnzahl(1);
	z.setName("name");
	z.setEinheit("g");
	zutaten.add(z);
	zutaten.add(z);
	return zutaten;
    }
}
