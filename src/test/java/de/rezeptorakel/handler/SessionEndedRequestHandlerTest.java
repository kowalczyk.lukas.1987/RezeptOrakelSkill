package de.rezeptorakel.handler;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class SessionEndedRequestHandlerTest {

    private SessionEndedRequestHandler sessionEndedRequestHandler = new SessionEndedRequestHandler();

    @Mock
    private HandlerInput handlerInput;

    @Test
    public void canhandle() throws Exception {
	try {
	    sessionEndedRequestHandler.canHandle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}

	when(handlerInput.matches(any())).thenReturn(true);
	assertTrue(sessionEndedRequestHandler.canHandle(handlerInput));

	when(handlerInput.matches(any())).thenReturn(false);
	assertFalse(sessionEndedRequestHandler.canHandle(handlerInput));
    }

    @Test
    public void handle() throws Exception {
	try {
	    sessionEndedRequestHandler.handle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}
	when(handlerInput.getResponseBuilder()).thenReturn(new ResponseBuilder());
	Optional<Response> handleResponse = sessionEndedRequestHandler.handle(handlerInput);
	assertTrue(handleResponse.isPresent());

    }

}
