package de.rezeptorakel.handler;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class HelpIntentHandlerTest {

    private HelpIntentHandler helpIntentHandler = new HelpIntentHandler();

    @Mock
    private HandlerInput handlerInput;

    @Test
    public void canhandle() throws Exception {
	try {
	    helpIntentHandler.canHandle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}

	when(handlerInput.matches(any())).thenReturn(true);
	assertTrue(helpIntentHandler.canHandle(handlerInput));

	when(handlerInput.matches(any())).thenReturn(false);
	assertFalse(helpIntentHandler.canHandle(handlerInput));
    }

    @Test
    public void handle() throws Exception {
	try {
	    helpIntentHandler.handle(null);
	    assertTrue(false);
	} catch (NullPointerException e) {
	    assertTrue(true);
	}
	ResponseBuilder value = new ResponseBuilder();
	when(handlerInput.getResponseBuilder()).thenReturn(value);
	Optional<Response> handleResponse = helpIntentHandler.handle(handlerInput);
	assertTrue(handleResponse.isPresent());

    }

}
