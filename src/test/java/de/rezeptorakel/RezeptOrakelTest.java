package de.rezeptorakel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RezeptOrakelTest {

    @Test
    void Init() {
	RezeptOrakel rezeptOrakel = new RezeptOrakel();
	assertNotNull(rezeptOrakel);
    }
    
    @Test
    void getBenoetigtePermissions() {
	assertNotNull(RezeptOrakel.getBenoetigtePermissions());
    }

}
