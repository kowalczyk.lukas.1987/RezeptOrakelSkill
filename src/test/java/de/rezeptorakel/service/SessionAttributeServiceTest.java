package de.rezeptorakel.service;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;

import de.rezeptorakel.datatype.SkillSessionAttributeNames;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class SessionAttributeServiceTest {
    @Mock
    private HandlerInput handlerInput;

    @Mock
    private HandlerUtilService handlerUtilService;

    private SessionAttributeService sessionAttributeService = SessionAttributeService.getImplementation();

    @Test
    void setSessionAttributes() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(sessionAttributeService,
		sessionAttributeService.getClass().getDeclaredField("handlerUtilService"), handlerUtilService);
	boolean erfolg = true;
	try {
	    sessionAttributeService.setSessionAttributes(null);

	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any())).thenReturn(null);
	    sessionAttributeService.setSessionAttributes(handlerInput);

	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesString());
	    sessionAttributeService.setSessionAttributes(handlerInput);

	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesIntegers());
	    sessionAttributeService.setSessionAttributes(handlerInput);

	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);
    }

    @Test
    void isSessionAttributEmpty() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(sessionAttributeService,
		sessionAttributeService.getClass().getDeclaredField("handlerUtilService"), handlerUtilService);

	boolean erfolg = true;
	try {
	    sessionAttributeService.setSessionAttributes(null);
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertTrue(sessionAttributeService.isSessionAttributEmpty(skillSessionAttributeNames));
	    }

	    sessionAttributeService.setSessionAttributes(handlerInput);
	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any())).thenReturn(null);
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertTrue(sessionAttributeService.isSessionAttributEmpty(skillSessionAttributeNames));
	    }

	    sessionAttributeService.setSessionAttributes(handlerInput);
	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesString());
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertTrue(sessionAttributeService.isSessionAttributEmpty(skillSessionAttributeNames));
	    }

	    sessionAttributeService.setSessionAttributes(handlerInput);
	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesIntegers());
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertFalse(sessionAttributeService.isSessionAttributEmpty(skillSessionAttributeNames));
	    }
	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);
    }

    @Test
    void putSessionAttribut() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(sessionAttributeService,
		sessionAttributeService.getClass().getDeclaredField("handlerUtilService"), handlerUtilService);

	boolean erfolg = true;
	try {
	    sessionAttributeService.setSessionAttributes(null);
	    Mockito.lenient().doNothing().when(handlerUtilService).setSessionAttributes(any(), any());
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		sessionAttributeService.putSessionAttribut(handlerInput, skillSessionAttributeNames, "value");
	    }

	    sessionAttributeService.setSessionAttributes(handlerInput);
	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any())).thenReturn(null);
	    Mockito.lenient().doNothing().when(handlerUtilService).setSessionAttributes(any(), any());
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		sessionAttributeService.putSessionAttribut(handlerInput, skillSessionAttributeNames, "value");
	    }

	    sessionAttributeService.setSessionAttributes(handlerInput);
	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesString());
	    Mockito.lenient().doNothing().when(handlerUtilService).setSessionAttributes(any(), any());
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		sessionAttributeService.putSessionAttribut(handlerInput, skillSessionAttributeNames, "value");
	    }

	    sessionAttributeService.setSessionAttributes(handlerInput);
	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesIntegers());
	    Mockito.lenient().doNothing().when(handlerUtilService).setSessionAttributes(any(), any());
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		sessionAttributeService.putSessionAttribut(handlerInput, skillSessionAttributeNames, new Integer(0));
	    }
	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);
	
    }

    @Test
    void getSessionAttribut() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(sessionAttributeService,
		sessionAttributeService.getClass().getDeclaredField("handlerUtilService"), handlerUtilService);

	boolean erfolg = true;
	try {
	    sessionAttributeService.setSessionAttributes(null);
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertNull(sessionAttributeService.getSessionAttribut(skillSessionAttributeNames));
	    }

	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any())).thenReturn(null);
	    sessionAttributeService.setSessionAttributes(handlerInput);
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertNull(sessionAttributeService.getSessionAttribut(skillSessionAttributeNames));
	    }

	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesString());
	    sessionAttributeService.setSessionAttributes(handlerInput);
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertEquals("value", sessionAttributeService.<String>getSessionAttribut(skillSessionAttributeNames));
	    }

	    Mockito.lenient().when(handlerUtilService.getSessionAttributes(any()))
		    .thenReturn(gefuellteSessionAttributesIntegers());
	    sessionAttributeService.setSessionAttributes(handlerInput);
	    for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
		assertEquals(new Integer(0), sessionAttributeService.getSessionAttribut(skillSessionAttributeNames));
	    }
	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);
    }

    private Map<String, Object> gefuellteSessionAttributesString() {
	HashMap<String, Object> sessionAttributes = new HashMap<>();
	for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
	    sessionAttributes.put(skillSessionAttributeNames.name(), "value");
	}
	return sessionAttributes;
    }

    private Map<String, Object> gefuellteSessionAttributesIntegers() {
	HashMap<String, Object> sessionAttributes = new HashMap<>();
	for (SkillSessionAttributeNames skillSessionAttributeNames : SkillSessionAttributeNames.values()) {
	    sessionAttributes.put(skillSessionAttributeNames.name(), new Integer(0));
	}
	return sessionAttributes;
    }
}
