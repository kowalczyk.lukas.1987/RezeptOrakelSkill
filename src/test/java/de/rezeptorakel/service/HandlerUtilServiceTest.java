package de.rezeptorakel.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.attributes.AttributesManager;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Intent;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.RequestEnvelope;
import com.amazon.ask.model.Slot;
import com.amazon.ask.model.services.ServiceClientFactory;
import com.amazon.ask.model.services.directive.DirectiveServiceClient;
import com.amazon.ask.model.services.ups.UpsServiceClient;
import com.amazon.ask.model.slu.entityresolution.Resolution;
import com.amazon.ask.model.slu.entityresolution.Resolutions;
import com.amazon.ask.model.slu.entityresolution.Value;
import com.amazon.ask.model.slu.entityresolution.ValueWrapper;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class HandlerUtilServiceTest {

    @Mock
    private DirectiveServiceClient directiveServiceClient;

    @Mock
    private UpsServiceClient upsService;

    @Mock
    private ServiceClientFactory serviceClientFactory;

    @Mock
    private AttributesManager attributesManager;

    @Mock
    private HandlerInput handlerInput;

    private HandlerUtilService handlerUtilService = HandlerUtilService.getImplementation();

    @Test
    void getSessionAttributes() {
	try {
	    handlerUtilService.getSessionAttributes(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getAttributesManager()).thenReturn(null);
	try {
	    handlerUtilService.getSessionAttributes(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getAttributesManager()).thenReturn(attributesManager);
	Mockito.lenient().when(attributesManager.getSessionAttributes()).thenReturn(null);
	try {
	    handlerUtilService.getSessionAttributes(handlerInput);
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}
    }

    @Test
    void setSessionAttributes() {
	try {
	    handlerUtilService.setSessionAttributes(null, null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getAttributesManager()).thenReturn(null);
	try {
	    handlerUtilService.setSessionAttributes(handlerInput, null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getAttributesManager()).thenReturn(attributesManager);
	Mockito.lenient().doNothing().when(attributesManager).setSessionAttributes(any());
	try {
	    handlerUtilService.setSessionAttributes(handlerInput, null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}
	try {
	    handlerUtilService.setSessionAttributes(null, new HashMap<String, Object>());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getAttributesManager()).thenReturn(null);
	try {
	    handlerUtilService.setSessionAttributes(handlerInput,  new HashMap<String, Object>());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getAttributesManager()).thenReturn(attributesManager);
	Mockito.lenient().doNothing().when(attributesManager).setSessionAttributes(any());
	try {
	    handlerUtilService.setSessionAttributes(handlerInput,  new HashMap<String, Object>());
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}
    }

    @Test
    void getSlots() {
	try {
	    handlerUtilService.getSlots(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(null);
	try {
	    handlerUtilService.getSlots(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeNullRequest());
	try {
	    handlerUtilService.getSlots(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeIntentNull());
	try {
	    handlerUtilService.getSlots(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeIntent());
	try {
	    handlerUtilService.getSlots(handlerInput);
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}
    }

    private RequestEnvelope getRequestEnvelopeNullRequest() {
	return RequestEnvelope.builder().build();
    }

    private RequestEnvelope getRequestEnvelopeIntentNull() {
	IntentRequest intentRequest = IntentRequest.builder().withIntent(null).build();
	return RequestEnvelope.builder().withRequest(intentRequest).build();
    }

    private RequestEnvelope getRequestEnvelopeIntent() {
	Intent intent = Intent.builder().build();
	IntentRequest intentRequest = IntentRequest.builder().withIntent(intent).build();
	return RequestEnvelope.builder().withRequest(intentRequest).build();
    }

    @Test
    void getListManagementServiceClient() {
	try {
	    handlerUtilService.getListManagementServiceClient(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(null);
	try {
	    handlerUtilService.getListManagementServiceClient(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	Mockito.lenient().when(serviceClientFactory.getListManagementService()).thenReturn(null);
	try {
	    handlerUtilService.getListManagementServiceClient(handlerInput);
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}
    }

    @Test
    void getAmazonDynamoDB() {
	try {
	    handlerUtilService.getAmazonDynamoDB();
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}

    }

    @Test
    void getUserEmail() {
	try {
	    handlerUtilService.getUserEmail(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getUserEmail(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	Mockito.lenient().when(serviceClientFactory.getUpsService()).thenReturn(null);
	try {
	    handlerUtilService.getUserEmail(handlerInput);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	Mockito.lenient().when(serviceClientFactory.getUpsService()).thenReturn(upsService);
	try {
	    handlerUtilService.getUserEmail(handlerInput);
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}
    }

    @Test
    void getMappedNameOfSlot() {
	try {
	    handlerUtilService.getMappedNameOfSlot(null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getMappedNameOfSlot(Slot.builder().build());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getMappedNameOfSlot(getSlotResolutionsPerAuthorityNull());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getMappedNameOfSlot(getSlotResolutionsPerAuthorityEmpty());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getMappedNameOfSlot(getSlotResolutionNull());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}
	try {
	    handlerUtilService.getMappedNameOfSlot(getSlotValueWrapperNull());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getMappedNameOfSlot(getSlotValueNull());
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.getMappedNameOfSlot(getSlotGefuellt());
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}
    }

    private Slot getSlotResolutionsPerAuthorityNull() {
	Resolutions resolutions = Resolutions.builder().build();
	return Slot.builder().withResolutions(resolutions).build();
    }

    private Slot getSlotResolutionsPerAuthorityEmpty() {
	Resolutions resolutions = Resolutions.builder().withResolutionsPerAuthority(new ArrayList<>()).build();
	return Slot.builder().withResolutions(resolutions).build();
    }

    private Slot getSlotResolutionNull() {
	ArrayList<Resolution> resolutionsPerAuthority = new ArrayList<>();
	resolutionsPerAuthority.add(Resolution.builder().build());
	Resolutions resolutions = Resolutions.builder().withResolutionsPerAuthority(resolutionsPerAuthority).build();
	return Slot.builder().withResolutions(resolutions).build();
    }

    private Slot getSlotValueWrapperNull() {
	ArrayList<Resolution> resolutionsPerAuthority = new ArrayList<>();
	resolutionsPerAuthority.add(Resolution.builder().withValues(null).build());
	Resolutions resolutions = Resolutions.builder().withResolutionsPerAuthority(resolutionsPerAuthority).build();
	return Slot.builder().withResolutions(resolutions).build();
    }

    private Slot getSlotValueNull() {
	ArrayList<Resolution> resolutionsPerAuthority = new ArrayList<>();
	List<ValueWrapper> values = new ArrayList<>();
	values.add(ValueWrapper.builder().withValue(null).build());
	resolutionsPerAuthority.add(Resolution.builder().withValues(values).build());
	Resolutions resolutions = Resolutions.builder().withResolutionsPerAuthority(resolutionsPerAuthority).build();
	return Slot.builder().withResolutions(resolutions).build();
    }

    private Slot getSlotGefuellt() {
	ArrayList<Resolution> resolutionsPerAuthority = new ArrayList<>();
	List<ValueWrapper> values = new ArrayList<>();
	values.add(ValueWrapper.builder().withValue(Value.builder().withName("test").build()).build());
	resolutionsPerAuthority.add(Resolution.builder().withValues(values).build());
	Resolutions resolutions = Resolutions.builder().withResolutionsPerAuthority(resolutionsPerAuthority).build();
	return Slot.builder().withResolutions(resolutions).build();
    }

    @Test
    void sendProgressiveResponse() {
	try {
	    handlerUtilService.sendProgressiveResponse(null, null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "test");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(null);
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeNullRequest());
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeRequestIdNull());
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeRequestIdGefuellt());
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeRequestIdGefuellt());
	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeRequestIdGefuellt());
	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	Mockito.lenient().when(serviceClientFactory.getDirectiveService()).thenReturn(directiveServiceClient);
	Mockito.lenient().doNothing().when(directiveServiceClient).enqueue(any());
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "TEST");
	    assertTrue(true);
	} catch (IllegalArgumentException e) {
	    assertTrue(false);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeRequestIdGefuellt());
	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	Mockito.lenient().when(serviceClientFactory.getDirectiveService()).thenReturn(directiveServiceClient);
	Mockito.lenient().doNothing().when(directiveServiceClient).enqueue(any());
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, "");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	Mockito.lenient().when(handlerInput.getRequestEnvelope()).thenReturn(getRequestEnvelopeRequestIdGefuellt());
	Mockito.lenient().when(handlerInput.getServiceClientFactory()).thenReturn(serviceClientFactory);
	Mockito.lenient().when(serviceClientFactory.getDirectiveService()).thenReturn(directiveServiceClient);
	Mockito.lenient().doNothing().when(directiveServiceClient).enqueue(any());
	try {
	    handlerUtilService.sendProgressiveResponse(handlerInput, null);
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

    }

    private RequestEnvelope getRequestEnvelopeRequestIdNull() {
	IntentRequest request = IntentRequest.builder().withRequestId(null).build();
	return RequestEnvelope.builder().withRequest(request).build();
    }

    private RequestEnvelope getRequestEnvelopeRequestIdGefuellt() {
	IntentRequest request = IntentRequest.builder().withRequestId("123").build();
	return RequestEnvelope.builder().withRequest(request).build();
    }
}
