package de.rezeptorakel.service;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazonaws.http.SdkHttpMetadata;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.amazonaws.services.simpleemail.model.SendEmailResult;

import de.rezeptorakel.datatype.Rezept;
import de.rezeptorakel.datatype.Zutat;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class EMailSendenServiceTest {
    @Mock
    private HandlerInput handlerInput;

    @Mock
    private HandlerUtilService handlerUtilService;

    @Mock
    private AmazonSimpleEmailService amazonSimpleEmailService;

    @Mock
    private SendEmailResult sendEmailResult;

    @Mock
    private SdkHttpMetadata sdkHttpMetadata;

    private EMailSendenService eMailSendenService = EMailSendenService.getImpementation();

    @Test
    void versendeRezeptUndEinkaufslisteRezeptGefuellt() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(eMailSendenService, eMailSendenService.getClass().getDeclaredField("handlerUtilService"),
		handlerUtilService);
	boolean exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), null, null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), new ArrayList<Zutat>(), null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn(null);
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn(null);
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), generateZutaten(), null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	// Positive RCs
	for (int i = 100; i <= 200; i++) {
	    boolean erfolg = true;
	    Integer rc = i;
	    try {
		Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
		Mockito.lenient().when(sdkHttpMetadata.getHttpStatusCode()).thenReturn(rc);
		Mockito.lenient().when(sendEmailResult.getSdkHttpMetadata()).thenReturn(sdkHttpMetadata);
		Mockito.lenient().when(amazonSimpleEmailService.sendEmail(any(SendEmailRequest.class)))
			.thenReturn(sendEmailResult);
		FieldSetter.setField(eMailSendenService,
			eMailSendenService.getClass().getDeclaredField("amazonSimpleEmailService"),
			amazonSimpleEmailService);
		eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), generateZutaten(), handlerInput);
	    } catch (Exception e) {
		erfolg = false;
	    }
	    assertTrue(erfolg);
	}

	// Negative RCs
	for (int i = 300; i <= 500; i++) {
	    exception = false;
	    Integer rc = i;
	    try {
		  Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
		Mockito.lenient().when(sdkHttpMetadata.getHttpStatusCode()).thenReturn(rc);
		Mockito.lenient().when(sendEmailResult.getSdkHttpMetadata()).thenReturn(sdkHttpMetadata);
		Mockito.lenient().when(amazonSimpleEmailService.sendEmail(any(SendEmailRequest.class)))
			.thenReturn(sendEmailResult);
		FieldSetter.setField(eMailSendenService,
			eMailSendenService.getClass().getDeclaredField("amazonSimpleEmailService"),
			amazonSimpleEmailService);
		eMailSendenService.versendeRezeptUndEinkaufsliste(generateRezept(), generateZutaten(), handlerInput);
	    } catch (Exception e) {
		exception = true;
	    }
	    assertTrue(exception);
	}
    }

    @Test
    void versendeRezeptUndEinkaufslisteRezeptLeer() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(eMailSendenService, eMailSendenService.getClass().getDeclaredField("handlerUtilService"),
		handlerUtilService);
	boolean exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), null, null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), new ArrayList<Zutat>(), null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn(null);
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(new Rezept(), generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);
    }

    @Test
    void versendeRezeptUndEinkaufslisteRezeptNull() throws NoSuchFieldException, SecurityException {
	FieldSetter.setField(eMailSendenService, eMailSendenService.getClass().getDeclaredField("handlerUtilService"),
		handlerUtilService);
	boolean exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, null, null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, new ArrayList<Zutat>(), null);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, new ArrayList<Zutat>(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, null, handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn(null);
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);

	exception = false;
	try {
	    Mockito.lenient().when(handlerUtilService.getUserEmail(any())).thenReturn("test@test.com");
	    eMailSendenService.versendeRezeptUndEinkaufsliste(null, generateZutaten(), handlerInput);
	} catch (Exception e) {
	    exception = true;
	}
	assertTrue(exception);
    }

    private Rezept generateRezept() {
	Rezept r = new Rezept();
	r.setAnleitung("anleitung");
	r.setId(1);
	r.setMahlzeit("mittag");
	r.setSchweregrad("einfach");
	r.setTitel("titel");
	r.setZutaten(generateZutaten());
	return r;
    }

    private List<Zutat> generateZutaten() {
	ArrayList<Zutat> zutaten = new ArrayList<>();
	Zutat z = new Zutat();
	z.setAnzahl(0);
	z.setEinheit("g");
	z.setName("test");
	zutaten.add(z);
	return zutaten;
    }

}
