package de.rezeptorakel.service;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.services.listManagement.AlexaList;
import com.amazon.ask.model.services.listManagement.AlexaListItem;
import com.amazon.ask.model.services.listManagement.AlexaListMetadata;
import com.amazon.ask.model.services.listManagement.AlexaListsMetadata;
import com.amazon.ask.model.services.listManagement.ListManagementServiceClient;

import de.rezeptorakel.datatype.Zutat;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class EinkaufslisteServiceTest {
    @Mock
    private HandlerUtilService handlerUtilService;

    @Mock
    private ListManagementServiceClient listManagmentServiceClient;

    @Mock
    private HandlerInput handlerInput;

    private EinkaufslisteService implementation = EinkaufslisteService.getImplementation();

    @Test
    void fuegeZurEinkaufslisteHinzu() throws NoSuchFieldException, SecurityException {

	FieldSetter.setField(implementation, implementation.getClass().getDeclaredField("handlerUtilService"),
		handlerUtilService);
	Mockito.lenient().when(handlerUtilService.getListManagementServiceClient(any()))
		.thenReturn(listManagmentServiceClient);

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any())).thenReturn(null);
	fuegZurEinkaufslisteHinzu();

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any()))
		.thenReturn(AlexaList.builder().build());
	fuegZurEinkaufslisteHinzu();

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any()))
		.thenReturn(gefuellteAlexaListMitFehler());
	fuegZurEinkaufslisteHinzu();

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any())).thenReturn(gefuellteAlexaList());
	fuegZurEinkaufslisteHinzu();

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any())).thenReturn(gefuellteAlexaList());
	Mockito.lenient().when(listManagmentServiceClient.getListsMetadata()).thenReturn(null);
	fuegZurEinkaufslisteHinzu();

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any())).thenReturn(gefuellteAlexaList());
	Mockito.lenient().when(listManagmentServiceClient.getListsMetadata()).thenReturn(getNullListe());
	fuegZurEinkaufslisteHinzu();

	Mockito.lenient().when(listManagmentServiceClient.getList(any(), any())).thenReturn(gefuellteAlexaList());
	Mockito.lenient().when(listManagmentServiceClient.getListsMetadata()).thenReturn(getGefuellteListe());
	fuegZurEinkaufslisteHinzu();
    }

    private AlexaListsMetadata getGefuellteListe() {
	ArrayList<AlexaListMetadata> ausg = new ArrayList<>();
	ausg.add(AlexaListMetadata.builder().withName("test").build());
	ausg.add(AlexaListMetadata.builder().withName("Alexa shopping list").build());
	return AlexaListsMetadata.builder().withLists(ausg).build();
    }

    private AlexaListsMetadata getNullListe() {
	return AlexaListsMetadata.builder().withLists(null).build();
    }

    private AlexaList gefuellteAlexaList() {
	return AlexaList.builder().addItemsItem(AlexaListItem.builder().withValue("ballala").build())
		.addItemsItem(AlexaListItem.builder().withValue("test 100 g").build())
		.addItemsItem(AlexaListItem.builder().withValue("random 1 mg").build()).build();
    }

    private AlexaList gefuellteAlexaListMitFehler() {
	return AlexaList.builder().addItemsItem(AlexaListItem.builder().withValue("ballala").build())
		.addItemsItem(AlexaListItem.builder().withValue("test").build()).build();
    }

    private void fuegZurEinkaufslisteHinzu() {
	boolean erfolg = true;
	try {
	    implementation.setListManagementServiceClient(null);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());
	    ArrayList<Zutat> generateNewZutaten = generateNewZutaten();
	    implementation.fuegeZurEinkaufslisteHinzu(generateNewZutaten);
	    ArrayList<Zutat> generateUpdateZutaten = generateUpdateZutaten();
	    implementation.fuegeZurEinkaufslisteHinzu(generateUpdateZutaten);

	    implementation.setListManagementServiceClient(handlerInput);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());
	    implementation.fuegeZurEinkaufslisteHinzu(generateNewZutaten);
	    implementation.fuegeZurEinkaufslisteHinzu(generateUpdateZutaten);

	    implementation.setListManagementServiceClient(handlerInput);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());
	    implementation.fuegeZurEinkaufslisteHinzu(generateNewZutaten);
	    implementation.fuegeZurEinkaufslisteHinzu(generateUpdateZutaten);

	    implementation.setListManagementServiceClient(handlerInput);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());
	    implementation.fuegeZurEinkaufslisteHinzu(generateNewZutaten);
	    implementation.fuegeZurEinkaufslisteHinzu(generateUpdateZutaten);

	} catch (Exception e) {
	    erfolg = false;
	}

	assertTrue(erfolg);
    }

    private ArrayList<Zutat> generateNewZutaten() {
	ArrayList<Zutat> arrayList = new ArrayList<Zutat>();
	Zutat zutat = new Zutat();
	zutat.setAnzahl(1);
	zutat.setEinheit("mg");
	zutat.setName("random");
	arrayList.add(zutat);
	return arrayList;
    }

    private ArrayList<Zutat> generateUpdateZutaten() {
	ArrayList<Zutat> arrayList = new ArrayList<Zutat>();
	Zutat zutat = new Zutat();
	zutat.setAnzahl(100);
	zutat.setEinheit("g");
	zutat.setName("test");
	arrayList.add(zutat);
	return arrayList;
    }

    @Test
    void shoppingID() {
	boolean erfolg = true;
	try {
	    FieldSetter.setField(implementation, implementation.getClass().getDeclaredField("handlerUtilService"),
		    handlerUtilService);

	    implementation.setListManagementServiceClient(handlerInput);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());

	    implementation.setListManagementServiceClient(handlerInput);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());

	    implementation.setListManagementServiceClient(handlerInput);
	    implementation.fuegeZurEinkaufslisteHinzu(null);
	    implementation.fuegeZurEinkaufslisteHinzu(new ArrayList<>());
	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);
    }

    @Test
    void setHandlerInput() throws NoSuchFieldException, SecurityException {
	boolean nullPointer = false;
	FieldSetter.setField(implementation, implementation.getClass().getDeclaredField("handlerUtilService"),
		handlerUtilService);
	try {
	    implementation.setListManagementServiceClient(null);
	} catch (Exception e) {
	    nullPointer = true;
	}
	assertFalse(nullPointer);
	boolean erfolg = true;
	try {
	    implementation.setListManagementServiceClient(handlerInput);
	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);
    }

    @Test
    void getImplementation() {
	assertNotNull(EinkaufslisteService.getImplementation());
    }
}
