package de.rezeptorakel.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class ProgressiveResponseServiceTest {
    @Mock
    private HandlerInput handlerInput;

    @Mock
    private HandlerUtilService handlerUtilService;

    private ProgressiveResponseService progressiveResponseService = ProgressiveResponseService.getImplementation();

    @Test
    void send() throws NoSuchFieldException, SecurityException {

	FieldSetter.setField(progressiveResponseService,
		progressiveResponseService.getClass().getDeclaredField("handlerUtilService"), handlerUtilService);
	boolean nullPointer = false;
	try {
	    progressiveResponseService.send(null, null);
	} catch (Exception e) {
	    nullPointer = true;
	}
	assert (nullPointer);

	nullPointer = false;
	try {
	    progressiveResponseService.send(null, "");
	} catch (Exception e) {
	    nullPointer = true;
	}
	assertTrue(nullPointer);

	nullPointer = false;
	try {
	    progressiveResponseService.send(handlerInput, null);
	} catch (Exception e) {
	    nullPointer = true;
	}
	assertTrue(nullPointer);

	nullPointer = false;
	try {
	    progressiveResponseService.send(handlerInput, "");
	} catch (Exception e) {
	    nullPointer = true;
	}
	assertTrue(nullPointer);

	boolean erfolg = true;
	try {
	    doNothing().when(handlerUtilService).sendProgressiveResponse(any(), any());
	    progressiveResponseService.send(handlerInput, "TEST");
	} catch (Exception e) {
	    erfolg = false;
	}
	assertTrue(erfolg);

    }

    @Test
    void getImplementation() {
	assertNotNull(ProgressiveResponseService.getImplementation());
    }
}
