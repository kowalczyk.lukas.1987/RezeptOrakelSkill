package de.rezeptorakel.service;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

import de.rezeptorakel.datatype.Mahlzeit;
import de.rezeptorakel.datatype.Rezept;
import de.rezeptorakel.datatype.Schweregrad;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class RezeptSucheServiceTest {
    @Mock
    private DynamoDBMapper dynamoDBMapper;

    private RezeptSucheService rezeptSucheService = RezeptSucheService.getImplementation();

    @Test
    void findeRezepte() throws NoSuchFieldException, SecurityException {

	boolean illegalArgumentException = false;
	try {
	    rezeptSucheService.findeRezepte(null, null);
	} catch (IllegalArgumentException e) {
	    illegalArgumentException = true;
	}

	assertTrue(illegalArgumentException);

	for (Mahlzeit mahlzeit : Mahlzeit.values()) {
	    boolean exception = false;
	    try {
		rezeptSucheService.findeRezepte(mahlzeit, null);
	    } catch (IllegalArgumentException e) {
		exception = true;
	    }
	    assertTrue(exception);
	}

	for (Schweregrad schweregrad : Schweregrad.values()) {
	    boolean exception = false;
	    try {
		rezeptSucheService.findeRezepte(null, schweregrad);
	    } catch (IllegalArgumentException e) {
		exception = true;
	    }
	    assertTrue(exception);
	}

	Mockito.lenient().when(dynamoDBMapper.scan(eq(Rezept.class), any(DynamoDBScanExpression.class)))
		.thenReturn(null);
	FieldSetter.setField(rezeptSucheService, rezeptSucheService.getClass().getDeclaredField("dynamoDBMapper"),
		dynamoDBMapper);
	for (Mahlzeit mahlzeit : Mahlzeit.values()) {
	    for (Schweregrad schweregrad : Schweregrad.values()) {
		boolean erfolg = true;
		try {
		    rezeptSucheService.findeRezepte(mahlzeit, schweregrad);
		} catch (Exception e) {
		    erfolg = false;
		}
		assertTrue(erfolg);
	    }

	}

	Mockito.lenient().when(dynamoDBMapper.scan(eq(Rezept.class), any(DynamoDBScanExpression.class)))
		.thenReturn(null);
	FieldSetter.setField(rezeptSucheService, rezeptSucheService.getClass().getDeclaredField("dynamoDBMapper"),
		dynamoDBMapper);
	for (Mahlzeit mahlzeit : Mahlzeit.values()) {
	    for (Schweregrad schweregrad : Schweregrad.values()) {
		assertNull(rezeptSucheService.findeRezepte(mahlzeit, schweregrad));
	    }

	}
    }

}
