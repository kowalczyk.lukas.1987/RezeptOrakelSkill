package de.rezeptorakel.datentype;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;

import de.rezeptorakel.datatype.SpeechText;
import de.rezeptorakel.datatype.SpeechTextPlaceHolder;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class SpeechTextTest {

    @Test
    void getSpeechTextTest() {
	try {
	    for (SpeechText speechText : SpeechText.values()) {
		assertNotNull(speechText.getSpeechText());
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    assertTrue(false);
	}

	for (SpeechText speechText : SpeechText.values()) {
	    for (SpeechTextPlaceHolder placeholder : SpeechTextPlaceHolder.values()) {
		try {
		    speechText.getSpeechText(placeholder, null);
		} catch (IllegalArgumentException e) {
		    assertTrue(true);
		}
	    }
	}
	for (SpeechText speechText : SpeechText.values()) {
	    for (SpeechTextPlaceHolder placeholder : SpeechTextPlaceHolder.values()) {
		try {
		    speechText.getSpeechText(placeholder, "");
		} catch (IllegalArgumentException e) {
		    assertTrue(true);
		}
	    }
	}

	rezepttitelPlaceholderTest(SpeechText.REZEPT_SENDEN_PROGRESSIV);
	rezepttitelPlaceholderTest(SpeechText.NAECHSTES_REZEPT_GEFUNDEN);
	rezepttitelPlaceholderTest(SpeechText.REZEPT_GEFUNEN);

	suchparameterPlaceholderTest(SpeechText.KEIN_REZEPT_GEFUNDEN);

	falsePlaceholderTest(SpeechText.EMAIL_SENDEN_FEHLER);
	falsePlaceholderTest(SpeechText.PERMISSION_FEHLET);
	falsePlaceholderTest(SpeechText.VERABSCHIEDEN);
	falsePlaceholderTest(SpeechText.HILFE);
	falsePlaceholderTest(SpeechText.WILLKOMMEN);
	falsePlaceholderTest(SpeechText.KEINE_WEITEREN_REZEPTE);
	falsePlaceholderTest(SpeechText.KEINE_REZEPTE_GEFUNDEN);
	falsePlaceholderTest(SpeechText.REZEPT_GESENDET);
	falsePlaceholderTest(SpeechText.REZEPT_SUCHE_PROGRESSIV);

    }

    private void suchparameterPlaceholderTest(SpeechText speechText) {
	try {
	    speechText.getSpeechText(SpeechTextPlaceHolder.REZEPTTITEL, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

	String ergebnis = speechText.getSpeechText(SpeechTextPlaceHolder.SUCHPARAMETER, "TEST");
	assertTrue(StringUtils.contains(ergebnis, "TEST"));
    }

    private void rezepttitelPlaceholderTest(SpeechText speechText) {
	String ergebnis = speechText.getSpeechText(SpeechTextPlaceHolder.REZEPTTITEL, "TEST");
	assertTrue(StringUtils.contains(ergebnis, "TEST"));

	try {
	    speechText.getSpeechText(SpeechTextPlaceHolder.SUCHPARAMETER, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}
    }

    private void falsePlaceholderTest(SpeechText speechText) {
	try {
	    speechText.getSpeechText(SpeechTextPlaceHolder.REZEPTTITEL, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}
	try {
	    speechText.getSpeechText(SpeechTextPlaceHolder.SUCHPARAMETER, "TEST");
	    assertTrue(false);
	} catch (IllegalArgumentException e) {
	    assertTrue(true);
	}

    }

}
